<?php

namespace App\Http\Controllers;

use App\Models\Ciudad;
use Illuminate\Http\Request;

class CiudadController extends Controller
{
    public function index(Request $request, string $slug) {
        /**
         * TODO:
         * Desarrollar un importador de CSV para tener base de datos de ciudades, sucursales, etc.
         * Al importar los datos, se debe generar un atributo slug que también debe ser guardado
         *
         * Generar sitemap.xml de forma dinamica usando la librería spatie/laravel-sitemap e implementar la interfaz a
         * los modelos que generar rutas como el ejemplo de ciudad aquí implementado.
         *
         * Estas dos tareas deberían ser implementadas en un comando de terminal con laravel:
         * https://laravel.com/docs/9.x/artisan#writing-commands
         */
        $ciudad = Ciudad::query()->where('slug', $slug)->firstOrFail();
        return view('index', compact('ciudad'));
    }
}
