<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sitemap\Contracts\Sitemapable;
use Spatie\Sitemap\Tags\Url;

class Ciudad extends Model implements Sitemapable
{
    use HasFactory;
    protected $table = 'ciudades';

    public function toSitemapTag(): Url|string|array
    {
        return route('ciudad.show', $this);
    }
}
